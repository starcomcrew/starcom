//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace starcom.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class k1
    {
        public int k1_id { get; set; }
        public short k1_firma { get; set; }
        public int k1_broj { get; set; }
        public int k1_stav { get; set; }
        public System.DateTime k1_data { get; set; }
        public string k1_konto { get; set; }
        public int k1_depar { get; set; }
        public int k1_vraboten { get; set; }
        public string k1_rabed { get; set; }
        public int k1_viddok { get; set; }
        public string k1_brojdok { get; set; }
        public Nullable<System.DateTime> k1_datadok { get; set; }
        public short k1_rok { get; set; }
        public long k1_arhbroj { get; set; }
        public short k1_podbr { get; set; }
        public short k1_valuta { get; set; }
        public string k1_zab { get; set; }
        public decimal k1_dolziden { get; set; }
        public decimal k1_pobden { get; set; }
        public decimal k1_dolzidev { get; set; }
        public decimal k1_pobdev { get; set; }
        public byte k1_god { get; set; }
        public string k1_user { get; set; }
        public Nullable<System.DateTime> k1_datu { get; set; }
    }
}
