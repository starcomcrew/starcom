﻿using starcom.Models.ViewModel;
using starcom.Models.EntityManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace starcom.Controllers
{
    public class SetupController : Controller
    {
        // GET: Setup
        public ActionResult AddCity()
        {
            
            var model = new CreateCityView() {
                states = StatesManager.GetStatesList()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult AddCity(CreateCityView cv)
        {
            if (ModelState.IsValid)
            {
                CitiesManager CM = new CitiesManager();
                CM.AddCity(cv);
            }
            var model = new CreateCityView()
            {
                states = StatesManager.GetStatesList()
            };
            return View(model);
        }
        public ActionResult AddState()
        {
            return View();
        }

    }
}