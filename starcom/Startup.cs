﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(starcom.Startup))]
namespace starcom
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
